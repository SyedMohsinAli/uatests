<head>
<title>UA Drug test kits, urine drug testing products and drugs test supplies by UATests.com</title>
<meta name="subject" content="UA Drug Test Kits | Drug Testing Products and Supplies by UATests">
<meta name="description" content="UA Drug test kits, urine drug testing products and drugs test supplies by UATests.com">
<meta name="keywords" content="ua, drug test, drug tests, drug testing, drugs test, drugs testing, drug, drugs, screen, urine, alcohol, kit, kits, Panels, Dips, Cup, Cups">
</head>

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
				
					<div class="row align-items-center">
				
						<div class="heading-block center m-0">
							<h1>Drug Test Kits, Drug Testing Supplies.</h1>
						</div>
	
							
						<span class="mx-auto">
								<div class="entry-content">
									<p>
									<span style="text-align:justify">A drug test is a device that can detect the presence of a drug or a number of drugs or their metabolized traces within human tissue or fluids. Drug tests can be divided into two general groups, <a href="#Federal Drug Testing">federally regulated drug testing</a> and non-federally regulated drug testing.
									<br>			
									When it comes to non-federally regulated testing, there are many  
											<a href="https://www.uatests.com/types-of-drug-tests/index.php" target="_blank">different kinds of drug tests</a>. With the increase of drug usage and abuse in America over the years, drug testing has increased as well, leading to a need for more
											<a href="https://www.uatests.com/drug-information/index.php" target="_blank">drug testing information</a>. Many people think that you have to go to a doctor&#8217;s office or clinic to 
											<a href="https://www.uatests.com/index.php" target="_blank">test for drugs</a>. However, practically every drug test can be purchased in the form of a
											<a href="https://www.uatests.com/types-of-drug-tests/home-drug-test.php" target="_blank">home drug test kit</a>, allowing you to 
											<a href="https://www.uatests.com/types-of-drug-tests">buy drug test kits</a> and perform a simple urine 
											<a href="https://www.uatests.com/types-of-drug-tests/home-drug-test.php" target="_blank">test for drugs from the comfort and convenience of your home</a>. The most popular forms of drug tests are UA Tests and Alcohol UA (UA is short for &quot;urine analysis&quot;, see 
											<a href="http://www.uatest.com">UA Test - urine analysis test</a> for more information on urine testing. UA Drug Testing is the most popular and proven way to detect drugs in human urine. The most popular and widely used drug testing is the Marijuana Drug Test. When it comes to UA Tests, saliva drug tests or any other form of drug testing, you will not find better tests or a better selection of drug tests than those offered by Transmetron! We guarantee it!
									</span>
									</p>
											
																									
									
								</div>
											
						</span>
					</div>
				</div>




				<div class="container">
					<div class="row posts-md col-mb-30">

						<div class="col-lg-3 col-md-6">
							<div class="entry">
								<div class="entry-image">
									<a href="#"><img src="images/magazine/thumb/1.jpg" alt="DRUG TEST SPECIALISTS!"></a>
								</div>
								<div class="entry-title title-xs nott">
									<h3><a href="">DRUG TEST SPECIALISTS!</a></h3>
								</div>

								<div class="entry-content">
									<p> 
																			
      								<p style="text-align:justify">
											Welcome to UATests.com - your one stop shop for drug test products, drug test supplies and drug testing information! Our online store offers 
											FDA approved drug tests at guaranteed low prices! Be sure to check out our 
											<a href="https://drug-test-store.com/">Drug Test Store</a>.
											</p>
      								<p>
											<a href="https://www.uatests.com/types-of-drug-tests/index.php" target="_blank">Drug Test Kits</a> 
											- We offer a wide selection of drug testing kits and supplies including:
											<a href="https://www.uatests.com/types-of-drug-tests/urine-test-kits/index.php" target="_blank">urine drug tests</a>,
											<a href="https://www.uatests.com/types-of-drug-tests/saliva-drug-test/index.php" target="_blank">saliva drug tests</a>,
											<a href="https://www.uatests.com/types-of-drug-tests/alcohol-drug-tests/index.php" target="_blank">alcohol tests</a> and a selection of
											<a href="https://www.uatests.com/other-urine-tests/index.php" target="_blank">other urine tests</a> as well!
											</p>
      								<p style="text-align:justify"><a href="https://www.uatests.com/types-of-drug-tests/home-drug-test.php" target="_blank">Home Drug Test</a> 
											- Our easy to use, easy to read home drug testing products are accurate, reliable and can be conveniently and privately done in your own home! 
											Nearly all of our product provide instant test results. Don't wait until it is too late drug test now and find out for sure!
											</p>
      								<p style="text-align:justify">
											<a href="https://www.uatests.com/drug-test-by-drug/thc-marijuana.php" target="_blank">Marijuana Drug Test</a> 
											- Our most popular drug test category. We offer a wide variety of marijuana drug test products in various test formats including 
											<a href="https://www.uatests.com/types-of-drug-tests/saliva-drug-test/index.php" target="_blank">oral drug test</a> and 
											<a href="https://www.uatests.com/drug-test-by-drug/thc-marijuana.php" target="_blank">marijuana urine drug test</a>. Initially Marijuana was tested only through urine drug test kits but now a much more simple drug test is available. Now Marijuana can be tested orally through the Saliva Drug Test! This Marijuana Saliva Test is non-intrusive, easy to administer and provides instant drug test results! This is the ideal drug test kit for employees, schools, or for individuals seeking to use it privately at home. We also offer the
      								<a href="https://www.uatests.com/types-of-drug-tests/hair-drug-test.php" target="_blank">Marijuana Hair Test</a> that can detect drug use up 
											to 90 days back (for head hair) and up to one year back (for body hair). 
											</p>						
									
									</p>
								</div>
							</div>
						</div>

						<div class="col-lg-3 col-md-6">
							<div class="entry">
								<div class="entry-image">
									<a href="#"><img src="images/magazine/thumb/2.jpg" alt="Products"></a>
								</div>
								<div class="entry-title title-xs nott">
									<h3><a href="">Products</a></h3>
								</div>
								
								<div class="entry-content">
	
											<p>Above is a mouth swab drug test, sometimes referred to as the &quot;Spit Drug Test&quot;. The OrAlert drug test detects 6 different drugs at one time (6 drug assay).</p>
											<p>There are 5 different common types of drug tests. They are as follows:</p>
											<p><a href="https://www.uatests.com/types-of-drug-tests/urine-test-kits/index.php" target="_blank">Urine Drug Test</a>: Detect the presence or absence of drugs and their metabolites in urine samples. These tests are often referred to as a UA test allowing you to do a urine test for drugs. </p>
											<p><a href="https://www.uatests.com/types-of-drug-tests/saliva-drug-test/index.php" target="_blank">Saliva Drug Test</a>: Detects the presence or absence of drugs and their metabolites in saliva samples.</p>

											<p><a href="https://www.uatests.com//types-of-drug-tests/alcohol-drug-tests/index.php" target="_blank">Alcohol Drug Test</a>: Detects the presence or absence of alcohol and its metabolite in urine, saliva or blood samples. These tests are sometimes called an alcohol UA and breath alcohol test.</p>

											<p><a href="https://www.uatests.com/types-of-drug-tests/hair-drug-test.php" target="_blank">Hair Drug Test</a>: Detects the presence or absence of drugs incased in a shaft of human hair or follicle.</p>

											<p><a href="https://www.uatests.com/types-of-drug-tests/nail-drug-test.php" target="_blank">Nail Drug Test</a>: Detects the presence or absence of drugs incased in the fingernail.</p>

											<p>Sweat Drug Test: Detects the presence of drugs through a sweat sample, most often using what is called a sweat patch.</p>
								</div>
							</div>
						</div>

						<div class="col-lg-3 col-md-6">
							<div class="entry">
								<div class="entry-image">
									<a href="#"><img src="images/magazine/thumb/3.jpg" alt="Drug Testing Methods"></a>
								</div>
								<div class="entry-title title-xs nott">
									<h3><a href="">What Are The Pros and Cons of Drug Testing Methods?</a></h3>
								</div>

								<div class="entry-content">
														
									<p>There are pros and cons to each drug testing method:</p>
								
																								
														<p><a href="https://www.uatests.com/types-of-drug-tests/urine-test-kits/index.php" target="_blank">Urine Drug Testing</a> (UA Testing):<br>
															PRO - very reliable<br>
															CON - viewed as being invasive
														</p>
														<p><a href="https://www.uatests.com/types-of-drug-tests/saliva-drug-test/index.php" target="_blank">Saliva Drug Testing</a>:<br>
															PRO - non invasive<br>
															CON - brief detection window
														</p>
														<p><a href="https://www.uatests.com/types-of-drug-tests/hair-drug-test.php" target="_blank">Hair Drug Testing</a>:<br>
															PRO - very long window of detection<br>
															CON - need a lab in order to obtain the results
														</p>
														<p><a href="https://www.uatests.com/types-of-drug-tests/sweat-drug-screen.php">Sweat Drug Testing</a>:<br>
															PRO - very easy to collect<br>
															CON - open to contamination
														</p>
																													
									<p>For more detailed information about the <a href="https://www.uatests.com/drug-testing-information/pros-cons-various-drug-testing-methods.php" target="_blank">Pros and Cons of Various Drug Testing Methods </a></p>

									<p><a href="https://www.uatests.com/other-urine-tests/smokers-nicotine-test.php" target="_blank">Nicotine Test</a> - is becoming quite popular today. The smokers nicotine test will test for metabolized nicotine and shows up as cotinine in the urine. We offer a 
									<a href="https://www.uatests.com/other-urine-tests/smokers-nicotine-test.php" target="_blank">urine cotinine test</a> that will determine nicotine use. More and more employers offering health insurance benefits are requiring cotinine testing for their employees. Parents concerned for their teenagers exposed to cigarette smoke often use the cotinine tests to help deter peer pressure.
									</p>
								</div>
									
							</div>
						</div>

						<div class="col-lg-3 col-md-6">
							<div class="entry">
								<div class="entry-image">
									<a href="#"><img src="images/magazine/thumb/4.jpg" alt="Federal Drug Testing - DOT Drug Test"></a>
								</div>
								<div class="entry-title title-xs nott">
									<h3><a href="">Federal Drug Testing - DOT Drug Test</a></h3>
								</div>

								<div class="entry-content">
									<p>
											Federal drug testing guidelines and processes are established and regulated (by the Substance Abuse and Mental Health Services Administration 
											or SAMHSA, formerly under the direction of theNational Institute on Drug Abuse or NIDA) require that companies who use professional drivers, 
											specified safety sensitive transportation and/or oil and gas related occupations, and certain federal employers, test them for the presence 
											of certain 
											<a title="drugs and drug information" href="https://www.uatests.com/drug-information/index.php" target="_blank">drugs</a>. These test classes 
											were established decades ago, and include five specific drug groups (including Marijuana (THC), Cocaine and Opiates). Drugs currently required 
											for testing by NIDA are referred to as the 
											<a href="https://www.uatests.com/drug-testing-information/nida-5.php" target="_blank">NIDA 5 drug test</a>. SAMHSA / DOT tests currently 
											exclude 
											<a href="http://www.hydrocodonedrugtest.com/" target="_blank">semi-synthetic opioids, such as oxycodone, oxymorphone, hydrocodone, hydromorphone</a>, 
											etc., and other prescription medications that are widely abused in the United States.
											</p>

											<p style="text-align:justify">
											We offer complete DOT drug testing services! We offer DOT Urine Drug Screen collections (mailed overnight to our SAMHSA certified lab) with 
											quick turnaround, quick results. We also offer DOT Breath Alcohol testing with our EBT. All of our collectors are thoroughly trained and 
											certified to do DOT collections. We offer 
											<a href="http://www.drugtestingcertification.com/" target="_blank">Drug Testing Certification - Drug Test School</a>.
									</p>

									<p align="center">For more information on employee drug tests and employer drug testing, see 
									<a href="https://www.uatests.com/drug-testing-information/employee-test.php" target="_blank">Detailed Information on Employee Drug Testing</a>.
									</p>					
								</div>
							</div>
						</div>

					</div>
				</div>
				
				
				<div class="container clearfix">
				
					<div class="row align-items-center">
				
						<div class="heading-block center m-0">
							<h2>Drug Test Kits, Drug Testing Supplies</h2>
						</div>
	
						<div class="w-100"></div>

						<div class="col-md-3">
							<h3>Drug Test for Drug Abuse</h3>
							Drug abuse is a major problem not only in the United States, but around the world and has indeed become a world wide epidemic! Now, to the rescue, comes the 
							<a href="http://www.instantdrugscreen.com" target="_blank">instant drug test kit</a>! Whatever may be the 
							<a href="https://www.uatests.com/types-of-drug-tests/index.php" target="_blank">type of drug test</a>, they help with the detection of drugs, which in turn forms the basis for a person returning to leading a drug free life. These tests can be done in total privacy and give extremely	accurate results. Drug tests are a realistic and affordable possibility these days with the availability of discount drug testing kits.
								<br>
								<br>
								<br>
								<br>
							<h3>Drug Testing</h3>
							It's as easy as 1... 2... 3...
								<br>
								<br>
							
										<ol>
											<li><b>Choose Your Tests</b>
											<ul>
												<li><a href="https://www.uatests.com/index.php" target="_blank">CLICK HERE for Drug Tests By Drug Type</a></li>
											</ul>
										</li>
										<br>
										<li>
										<b>Purchase Tests</b>
										<ul>
											<li>quality, FDA Approved tests</li>
											<li>easy to order</li>
											<li>place your secure credit card order</li>
										</ul>
										</li>
										<br>
										<li>
										<b>Receive &amp; Administer</b>
										<ul>
											<li>tests are shipped within 24 hours of payment receipt</li>
											<li>Express Mail (next day air) option</li>
											<li>tests are easy to use</li>
											<li>instant results (no waiting)</li>
											<li>results are easy to read and interpret</li>
										</ul>
										</li>
									</ol>
									<a href="https://www.uatests.com/super-special-deals.php" target="_blank">Super Special Deals on Drug Test Kits</a></h2>
						</div>

						<div class="col-md-9">
							<h2>Types Of Drug Tests</h2>

							<p>
							Various types of tests such as  
							<a title="Urine Drug Test" href="https://www.uatests.com/types-of-drug-tests/urine-test-kits/index.php" target="_blank">urine drug test</a>, 
							<a title="Saliva Drug Test" href="https://www.uatests.com/types-of-drug-tests/saliva-drug-test/index.php" target="_blank">saliva drug test</a> and 
							<a title="Hair Drug Test" href="https://www.uatests.com/types-of-drug-tests/hair-drug-test.php" target="_blank">hair follicle drug test</a> are 
							administered in order to 
							<a href="https://www.uatests.com/drug-testing-information/drug-detection-times.php" target="_blank">detect drug abuse</a>. Among the most 
							commonly used drug tests is the urine test and you can find many 
							<a title="Urine Drug Testing Kits" href="https://www.uatests.com/types-of-drug-tests/urine-test-kits/index.php" target="_blank">urine drug 
							testing kits</a> available at a wide range of pharmacies these days.The drawbacks of purchasing from a pharmacy is twofold. First, their test 
							kits are extremely expensive (a single panel THC drug test will cost $13 or more each! Whereas, we charge only $2.95 for a single 
							<a href="https://www.uatests.com/drug-test-by-drug/thc-marijuana.php" target="_blank">THC Home Drug Test</a>). And, we have the Guaranteed Lowest 
							Prices on the internet! Second, pharmacies have a very limited selection of tests. Typically a local drugstore will only carry about 3 or 4 
							different types of drug tests at the most (many only carry Marijuana tests). Whereas, we carry perhaps the largest selection of drug test kits 
							on the internet today. With this large selection of drug tests, you can find out the presence or absence of nearly any specific drug. One of 
							the major advantages with the urine drug testing kits is that up to 
							<a href="https://www.uatests.com/types-of-drug-tests/urine-test-kits/dip-tests.php#12panel" target="_blank">12 different drugs can be detected using a single drug test kit</a>. 
							With this all inclusive drug test kit, you can rest positive knowing if they are clean and drug free.
							</p>

							<p>
							In many instances 
							<a title="Saliva Drug Test" href="https://www.uatests.com/types-of-drug-tests/saliva-drug-test/index.php" target="_blank">saliva drug test kits</a> 
							are used as a non-invasive way to drug test employees, family members and even self drug testing. This drug test for saliva works by 
							detecting 
							<a href="https://www.uatests.com/drug-testing-information/drug-detection-times.php" target="_blank">traces of certain drugs</a> found in a 
							person's mouth, specifically in the saliva, sometimes called spit. This test is often referred to as the spit test or mouth swab drug test. 
							The results of saliva tests are almost always reliable with no 
							<a href="https://www.uatests.com/drug-testing-information/altered-urine-sample.php" target="_blank">known adulterants</a> and it is therefore 
							a method that is used extensively for drug tests (preferred drug testing). A saliva drug test is also easy to conduct and allows for the 
							detection of six different types of drugs. These saliva tests now come in two different configurations, one testing for cocaine, amphetamines, 
							methamphetamines, opiates, marijuana and phencyclidine. The 
							<a href="https://www.uatests.com/types-of-drug-tests/oralert.php" target="_blank">new OrAlert test</a> replaces the 
							<a href="https://www.uatests.com/drug-information/pcp.php" target="_blank">PCP drug</a> with 
							<a href="https://www.uatests.com/drug-information/benzodiazepines.php" target="_blank">benzodiazepines</a>. The oral saliva drug test can be 
							done almost anywhere and in total privacy, making it among the preferred methods of drug testing.
							</p>

							<p>
							Similarly you can find many 
							<a href="https://www.uatests.com/types-of-drug-tests/hair-drug-test.php" target="_blank">hair follicle drug testing kits</a> that can help 
							with 
							<a href="https://www.uatests.com/drug-testing-information/drug-detection-times.php" target="_blank">detection of certain drugs</a>. Hair 
							follicles (actually the hair shaft itself) is known to store traces of drugs for a fairly long period of time (much longer than urine or 
							saliva tests). The hair drug test can reveal drug use as far back as 90 days (using 1.5 inches of head hair or up to 12 months (using body 
							hair). Using this method one can find out a lot of details about a particular person's drug abuse history. 
							</p>

							<p>Most of the drug testing kits that are available these days are easy to use. The list of drugs that can be detected using them include 
							<a title="Opiate Drug Test" href="https://www.uatests.com/drug-test-by-drug/opiate.php" target="_blank">opiates</a> | 
							<a title="Cocaine Drug Test" href="https://www.uatests.com/drug-test-by-drug/cocaine.php" target="_blank">cocaine</a> | 
							<a title="Marijuana Drug Test" href="https://www.uatests.com/drug-test-by-drug/thc-marijuana.php" target="_blank">marijuana</a> | 
							<a title="Methamphetamine Drug Test" href="https://www.uatests.com/drug-test-by-drug/methamphetamines-meth.php" target="_blank">methamphetamines</a> | 
							<a href="https://www.uatests.com/drug-information/amphetamine.php" target="_blank">amphetamines</a> and 
							<a href="https://www.uatests.com/drug-information/benzodiazepines.php" target="_blank">benzodiazepines</a> to mention only a few. There are also a 
							wide range of testing kits that can be used for 
							<a href="https://www.uatests.com/other-urine-tests/smokers-nicotine-test.php" target="_blank">detecting tobacco smoke (nicotine)</a> and 
							alcohol. We offer the 
							<a href="https://www.uatests.com/other-urine-tests/smokers-nicotine-test.php" target="_blank">smoker's nicotine test</a> and 
							<a href="https://www.uatests.com/types-of-drug-tests/alcohol-drug-tests/index.php" target="_blank">two types of alcohol tests</a>: 
							<a title="Breath Alcohol Test" href="https://www.uatests.com/types-of-drug-tests/alcohol-drug-tests/breath-alcohol-test.php" target="_blank">breath alcohol</a> and 
							<a title="Saliva Alcohol Test" href="https://www.uatests.com/types-of-drug-tests/alcohol-drug-tests/saliva-alcohol-test.php" target="_blank">saliva alcohol tests</a>. 
							These tests are a great resource for and especially in the case of minors. Often an anxious parent wants to know whether their child is under 
							the influence of drugs or alcohol. Administering these easy to use drug tests on their child is the best way they can find out about his or 
							her drug habit. We provide an extensive list of
							<a href="https://www.uatests.com/drug-testing-information/parent-drug-resource/index.php" target="_blank">drug test resources for parents</a>. 
							</p>

							<p>
							We carry a full line of home drug test kits and supplies. The most popular home test is the urine drug test (Marijuana drug test) and is 
							perfect for in home use. We provide discount home drug kits to parents because we know that drug testing can help save lives! There are many 
							reasons to buy a 
							<a href="https://www.uatests.com/types-of-drug-tests/home-drug-test.php" target="_blank">urine home drug test</a> kit or a saliva home drug 
							test kit, but no matter what the reason, it is important that the test be accurate. You can rest assured that we sell the most reliable home 
							test kits available. All of the drug tests we sell are professional grade drug tests that are used in professional drug testing clinics and 
							hospitals. Therefore, when you use one of our urine drug tests in the privacy of your own home, you have the assurance that the drug test 
							results will be accurate every time.
							</p>
						</div>
					</div>
				</div>				
	
			</div>
		</section><!-- #content end -->

		

	