<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-1817692-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-1817692-1');
</script>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/swiper.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

<meta name="author" content="Steve Sandlin">
<meta name="Geography" content="1725 South 1700 East, Salt Lake City, UT 84108">
<meta name="Language" content="English">
<meta name="Copyright" content="Transmetron">
<meta name="Designer" content="Steve Sandlin">
<meta name="Publisher" content="Transmetron">
<meta name="city" content="Salt Lake City">
<meta name="country" content="USA">
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/php; charset=utf-8">
<meta name="distribution" content="global">
<meta name="expires" content="never">
<meta name="Revisit-After" content="7 days">
<meta name="robots" content="index,follow">






<style>
		@media (min-width: 992px) {

			#top-bar.transparent-topbar {
				z-index: 399;
				border-bottom: 0;
				margin-top: 15px;
			}

			#top-bar.transparent-topbar {
				background: transparent !important;
			}

			#top-bar + #header.transparent-header.floating-header {
				margin-top: 15px;
			}

		}

		.dark .top-links li > a,
		.dark #top-social li a { color: #FFF; }

		.dark .top-links li:hover { background-color: rgba(255,255,255,0.15); }

		.dark #top-social li { border-left: 0; }
	</style>


</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
	
	
	<!-- Top Bar
		============================================= -->
		<div id="top-bar" class="transparent-topbar dark">
			<div class="container">

				<div class="row justify-content-between">
					<div class="col-12 col-md-auto">

						<!-- Top Links
						============================================= -->
						<div class="top-links">
							<ul class="top-links-container">
								<li class="top-links-item"><a href="index.php">Home</a></li>
								<li class="top-links-item"><a href="https://drug-test-store.com">Buy A Drug Test</a></li>
								<li class="top-links-item"><a href="faqs.php">FAQs</a></li>
								<li class="top-links-item"><a href="contact.php">Contact Us</a>
								</li>
							</ul>
						</div><!-- .top-links end -->

					</div>

					<div class="col-12 col-md-auto">

						<!-- Top Social
						============================================= -->
						<ul id="top-social">
							<li><a href="#" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
							<li><a href="tel:+1.801.5962709" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">+1-801-596-2709</span></a></li>
							<li><a href="mailto:instantdrugcreen@gmail.com" class="si-email3"><span class="ts-icon"><i class="icon-envelope-alt"></i></span><span class="ts-text">InstantDrugScreen@gmail.com</span></a></li>
						</ul><!-- #top-social end -->

					</div>
				</div>

			</div>
		</div><!-- #top-bar end -->
	
	
	
	
	

		<!-- Header
		============================================= -->
		<header id="header" class="transparent-header floating-header">
			<div id="header-wrap">
				<div class="container">
					<div class="header-row">

						<!-- Logo
						============================================= -->
						<div id="logo">
							<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="UATests.com"></a>
							<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="UATests.com"></a>
						</div><!-- #logo end -->

						<div class="header-misc">

							<!-- Top Search
							============================================= -->
							<div id="top-search" class="header-misc-icon">
								<a href="#" id="top-search-trigger"><i class="icon-line-search"></i><i class="icon-line-cross"></i></a>
							</div><!-- #top-search end -->

						</div>

						<div id="primary-menu-trigger">
							<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
						</div>

						<!-- Primary Navigation
						============================================= -->
						<nav class="primary-menu">

							<ul class="menu-container">
															
								<li class="menu-item">
									<a class="menu-link" href=""><div>Drug Tests</div></a>
									<ul class="sub-menu-container">
																									
										<li class="menu-item">
											<a class="menu-link" href=""><div>Alcohol Tests</div></a>
											<ul class="sub-menu-container">
												
												<li class="menu-item">
													<a class="menu-link" href=""><div>Breath Alcohol</div></a>
													<ul class="sub-menu-container">
														<li class="menu-item">
															<a class="menu-link" href=""><div>BreathScan</div></a>
														</li>
														<li class="menu-item">
															<a class="menu-link" href=""><div>Mission Breath Test</div></a>
														</li>
													</ul>
												</li>
												<li class="menu-item">
													<a class="menu-link" href=""><div>Breath Alcohol Machines</div></a>
													<ul class="sub-menu-container">
														<li class="menu-item">
															<a class="menu-link" href=""><div>AlcoHawk Slim</div></a>
														</li>
														<li class="menu-item">
															<a class="menu-link" href=""><div>Intoximeter</div></a>
														</li>
														<li class="menu-item">
															<a class="menu-link" href=""><div>Lifeloc</div></a>
														</li>
													</ul>
												</li>
												
												<li class="menu-item">
													<a class="menu-link" href=""><div>EtG Urine Test</div></a>
												</li>
																				
												<li class="menu-item">
													<a class="menu-link" href=""><div>Saliva Alcohol Tests</div></a>
													<ul class="sub-menu-container">
														<li class="menu-item">
															<a class="menu-link" href=""><div>AlcoScreen</div></a>
														</li>
														<li class="menu-item">
															<a class="menu-link" href=""><div>Mission Saliva Test</div></a>
														</li>
														<li class="menu-item">
															<a class="menu-link" href=""><div>QED Alcohol Test</div></a>
														</li>
													</ul>
												</li>
												<li class="menu-item">
													<a class="menu-link" href=""><div>Urine Alcohol Test</div></a>
												</li>
											</ul>
										</li>
																													
										<li class="menu-item">
											<a class="menu-link" href=""><div>CLIA Waived - NIDA 5 Drug Test</div></a>
										</li>
										<li class="menu-item">
											<a class="menu-link" href=""><div>Drug Test Controls</div></a>
										</li>
										
										<li class="menu-item">
											<a class="menu-link" href=""><div>Hair Drug Test</div></a>
											<ul class="sub-menu-container">
												<li class="menu-item">
													<a class="menu-link" href=""><div>Hair Confirm Drug Test</div></a>
												</li>	
												<li class="menu-item">
													<a class="menu-link" href=""><div>Hair Lab Test</div></a>
												</li>
											</ul>
										</li>
																																				
										
										<li class="menu-item">
											<a class="menu-link" href=""><div>Home Drug Test</div></a>
										</li>
																										
										<li class="menu-item">
											<a class="menu-link" href=""><div>Lab Tests</div></a>
											<ul class="sub-menu-container">
												<li class="menu-item">
													<a class="menu-link" href=""><div>600 Brand Name Prescription Drugs</div></a>
												</li>	
												<li class="menu-item">
													<a class="menu-link" href=""><div>GCMS Laboratory Services</div></a>
												</li>												<li class="menu-item">
													<a class="menu-link" href=""><div>Laboratory Confirmation</div></a>
												</li>												<li class="menu-item">
													<a class="menu-link" href=""><div>Lab Hair Test</div></a>
												</li>												<li class="menu-item">
													<a class="menu-link" href=""><div>Laboratory Services</div></a>
												</li>												<li class="menu-item">
													<a class="menu-link" href=""><div>LCMS Laboratory Services</div></a>
												</li>
											</ul>
										</li>
										
										
										<li class="menu-item">
											<a class="menu-link" href=""><div>Nail Drug Test</div></a>
										</li>
																									
										<li class="menu-item">
											<a class="menu-link" href=""><div>Saliva Drug Tests</div></a>
											<ul class="sub-menu-container">
												<li class="menu-item">
													<a class="menu-link" href=""><div>Flip Top Saliva Test</div></a>
												</li>
												<li class="menu-item">
													<a class="menu-link" href=""><div>GEN Cube</div></a>
												</li>												
												<li class="menu-item">
													<a class="menu-link" href=""><div>Oral Cube Drug Test</div></a>
												</li>	
												<li class="menu-item">
													<a class="menu-link" href=""><div>Oralert</div></a>
												</li>	
												<li class="menu-item">
													<a class="menu-link" href=""><div>Oral Fluid Screen</div></a>
												</li>	
												<li class="menu-item">
													<a class="menu-link" href=""><div>Oratect</div></a>
												</li>	
												<li class="menu-item">
													<a class="menu-link" href=""><div>Single Panel Saliva</div></a>
												</li>	
												<li class="menu-item">
													<a class="menu-link" href=""><div>Statswab</div></a>					
												</li>
												<li class="menu-item">
													<a class="menu-link" href=""><div>T-Cube</div></a>
												</li>
											</ul>
										</li>
										
										
										<li class="menu-item">
											<a class="menu-link" href=""><div>Steroids Drug Test</div></a>
										</li>
										<li class="menu-item">
											<a class="menu-link" href=""><div>Substance/Surface Drug Test</div></a>
										</li>
										<li class="menu-item">
											<a class="menu-link" href=""><div>Sweat Drug Test</div></a>
										</li>										
										
										<li class="menu-item">
											<a class="menu-link" href=""><div>Urine Drug Tests</div></a>
											<ul class="sub-menu-container">
												
												<li class="menu-item">
													<a class="menu-link" href=""><div>Cassette Tests</div></a>
													</li>	
												<li class="menu-item">
													<a class="menu-link" href=""><div>CLIA Waived - NIDA 5 Test</div></a>
													</li>	
												<li class="menu-item">
													<a class="menu-link" href=""><div>Dip Drug Tests</div></a>
													</li>	
												<li class="menu-item mega-menu">
													<a class="menu-link" href=""><div>Drug Test Cups</div></a>
													
																				
														<div class="mega-menu-content">
															<div class="container">
																<div class="row">
																<ul class="sub-menu-container mega-menu-column col">
																	<li class="menu-item">
																		<a class="menu-link" href=""><div>Chem True</div></a>
																	</li>
																	<li class="menu-item">
																		<a class="menu-link" href=""><div>Eco Cup</div></a>
																	</li>
																	<li class="menu-item">
																		<a class="menu-link" href=""><div>EZ Click</div></a>
																	</li>
																</ul>
																<ul class="sub-menu-container mega-menu-column col">
																	<li class="menu-item">
																		<a class="menu-link" href=""><div>EZ Split</div></a>
																	</li>
																	<li class="menu-item">
																		<a class="menu-link" href=""><div>GEN Cup</div></a>
																	</li>
																	<li class="menu-item">
																		<a class="menu-link" href=""><div>iCup</div></a>
																	</li>
												
																</ul>
																<ul class="sub-menu-container mega-menu-column col">
																	<li class="menu-item">
																		<a class="menu-link" href=""><div>Magenta</div></a>
																	</li>
																	<li class="menu-item">
																		<a class="menu-link" href=""><div>T-Cup</div></a>
																	</li>
																	<li class="menu-item">
																		<a class="menu-link" href=""><div>VistaFlow</div></a>
																	</li>
													
																</ul>
												
															</div>
														</div>
													
													
													
													
													
													</li>	
												<li class="menu-item">
													<a class="menu-link" href=""><div>Drug Test with Urine Adulteration</div></a>
													</li>	
											
											</ul>
										</li>
										<li class="menu-item">
											<a class="menu-link" href=""><div>Urine Adulteration Test</div></a>
										</li>										
									</ul>
								</li>
	
								<li class="menu-item mega-menu">
									<a class="menu-link" href=""><div>Test By Drug</div></a>
									<div class="mega-menu-content">
										<div class="container">
											<div class="row">
												<ul class="sub-menu-container mega-menu-column col">
													<li class="menu-item">
														<a class="menu-link" href=""><div>Alcohol Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Amphetamine Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Barbiturates Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Bath Salts Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Benzoiazepines Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Cocaine Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>EtG Test</div></a>
													</li>
												</ul>
												<ul class="sub-menu-container mega-menu-column col">
													<li class="menu-item">
														<a class="menu-link" href=""><div>Nicotine Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Fentanyl Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Gabapentin Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>GHB Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Heroin Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Ketamine Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Kratom Test</div></a>
													</li>
												
												</ul>
												<ul class="sub-menu-container mega-menu-column col">
													<li class="menu-item">
														<a class="menu-link" href=""><div>LSD Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>MDMA (Ecstasy) Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Methadone Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Methamphetamines Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Methaqualone Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Morphine Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Opiate Test</div></a>
													</li>
													
												</ul>
												<ul class="sub-menu-container mega-menu-column col">
													<li class="menu-item">
														<a class="menu-link" href=""><div>Oxycodone Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>PCP Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>PPX Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Prescription Drugs Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Soma Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Steroids Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Suboxone Test</div></a>
													</li>
													<li class="menu-item">
										
												</ul>
												<ul class="sub-menu-container mega-menu-column col">
													<li class="menu-item">
														<a class="menu-link" href=""><div>Synthetic Stimulants Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>TCA Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href="l"><div>THC (Marijuana) Test</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Tramadol Test</div></a>
													</li>
													
												</ul>
											</div>
										</div>
									</div>
								</li>
								
								<li class="menu-item">
									<a class="menu-link" href=""><div>Testing Supplies</div></a>
									<ul class="sub-menu-container">
										<li class="menu-item">
											<a class="menu-link" href=""><div>Gloves</div></a>
										</li>
										<li class="menu-item">
											<a class="menu-link" href=""><div>Testing Forms</div></a>
										
										</li>
										<li class="menu-item">
											<a class="menu-link" href=""><div>Misc. Supplies</div></a>
											
										</li>
									</ul>
								</li>
							
							<li class="menu-item">
									<a class="menu-link" href=""><div>Testing Information</div></a>
									<ul class="sub-menu-container">
										<li class="menu-item">
											<a class="menu-link" href=""><div>Aids Testing</div></a>
										</li>
										<li class="menu-item">
											<a class="menu-link" href=""><div>Altered Urine Sample</div></a>
											<ul class="sub-menu-container">
												<li class="menu-item">
													<a class="menu-link" href=""><div>Urine Adulteration Test</div></a>
												</li>
												<li class="menu-item">
													<a class="menu-link" href=""><div>What Is Urine Adulteration?</div></a>
												</li>	
											</ul>
										<li class="menu-item">
											<a class="menu-link" href=""><div>Breath Alcohol Testing</div></a>
											<ul class="sub-menu-container">
												<li class="menu-item">
													<a class="menu-link" href=""><div>Breath Alcohol Calculator</div></a>
												</li>
												<li class="menu-item">
													<a class="menu-link" href=""><div>Breathalyzer</div></a>
												</li>	
											</ul>											
										<li class="menu-item">
											<a class="menu-link" href=""><div>CLIA Waived</div></a>
										</li>
									
										<li class="menu-item">
											<a class="menu-link" href=""><div>Drug Detection Times</div></a>
											<ul class="sub-menu-container">
												<li class="menu-item">
													<a class="menu-link" href=""><div>Saliva Drug Detection Window</div></a>
												</li>
											</ul>														
										</li>	
										
										<li class="menu-item">
											<a class="menu-link" href=""><div>Drug Use Information</div></a>
											<ul class="sub-menu-container">
												<li class="menu-item">
													<a class="menu-link" href=""><div>Drugs of Abuse</div></a>
												</li>
												<li class="menu-item">
													<a class="menu-link" href=""><div>Over The Counter Drug Abuse</div></a>
												</li>												
												<li class="menu-item">
													<a class="menu-link" href=""><div>Substance Abuse Warning Signs</div></a>
												</li>												
											</ul>														
										</li>											
										
										
										
										
										<li class="menu-item">
											<a class="menu-link" href=""><div>Drug Test Training - Drug Testing Certification</div></a>
										</li>										
										<li class="menu-item">
											<a class="menu-link" href=""><div>Employer Drug Testing</div></a>
											<ul class="sub-menu-container">
												<li class="menu-item">
													<a class="menu-link" href=""><div>Pre-employment Drug Test</div></a>
												</li>
											</ul>														
										</li>			
										<li class="menu-item">
											<a class="menu-link" href=""><div>FAQS (frequently asked questions)</div></a>
											<ul class="sub-menu-container">
												<li class="menu-item">
													<a class="menu-link" href=""><div>Detox - Pass A Drug Test</div></a>
												</li>											
												<li class="menu-item">
													<a class="menu-link" href=""><div>Drug Slang Terms</div></a>
												</li>
												<li class="menu-item">
													<a class="menu-link" href=""><div>Drug Testing Terms (glossary)</div></a>
												</li>
												<li class="menu-item">
													<a class="menu-link" href=""><div>How To Use Drug Tests</div></a>
												</li>	
											</ul>
										</li>
										<li class="menu-item">
											<a class="menu-link" href=""><div>Hair Drug Testing</div></a>
											<ul class="sub-menu-container">
												<li class="menu-item">
													<a class="menu-link" href=""><div>Hair Confirm FAQ</div></a>
												</li>
												<li class="menu-item">
													<a class="menu-link" href=""><div>Hair Drug Test FAQ</div></a>
												</li>	
											</ul>
										</li>
									
										<li class="menu-item">
											<a class="menu-link" href=""><div>Laboratory Confirmation</div></a>
										</li>
										<li class="menu-item">
											<a class="menu-link" href=""><div>NIDA 5</div></a>
										</li>
										<li class="menu-item">
											<a class="menu-link" href=""><div>Other Drug Testing Resources</div></a>
											<ul class="sub-menu-container">
												<li class="menu-item">
													<a class="menu-link" href=""><div>Drug Test News</div></a>
												</li>
											</ul>
										</li>
										<li class="menu-item">
											<a class="menu-link" href=""><div>Parent's Drug Resources</div></a>
											<ul class="sub-menu-container">
												<li class="menu-item">
													<a class="menu-link" href=""><div>Drug Free Youth</div></a>
												</li>
											</ul>
										</li>										

										<li class="menu-item">
											<a class="menu-link" href=""><div>Pros and Cons of Various Drug Testing Methods</div></a>
										</li>										
									
										<li class="menu-item">
											<a class="menu-link" href=""><div>Test Specifications</div></a>
										</li>										
									</ul>	
								</li>		

								<li class="menu-item mega-menu">
									<a class="menu-link" href="#"><div>Drug Information</div></a>
									<div class="mega-menu-content">
										<div class="container">
											<div class="row">
												<ul class="sub-menu-container mega-menu-column col">
												<li class="menu-item">
														<a class="menu-link" href=""><div>Alcohol</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Amphetamine</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Barbiturates</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Bath Salts</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Benzoiazepines</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Cocaine</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>EtG</div></a>
													</li>
												</ul>
												<ul class="sub-menu-container mega-menu-column col">
													<li class="menu-item">
														<a class="menu-link" href=""><div>Nicotine</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Fentanyl</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Gabapentin</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>GHB</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Heroin</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Ketamine</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Kratom</div></a>
													</li>
													
												</ul>
												<ul class="sub-menu-container mega-menu-column col">
													<li class="menu-item">
														<a class="menu-link" href=""><div>LSD</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>MDMA (Ecstasy)</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Methadone</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Methamphetamines</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Methaqualone</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Morphine</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Opiate</div></a>
													</li>
													
												</ul>
												<ul class="sub-menu-container mega-menu-column col">
													<li class="menu-item">
														<a class="menu-link" href=""><div>Oxycodone</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>PCP</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>PPX</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Prescription Medications</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Soma</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Steroids</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Suboxone</div></a>
													</li>
													<li class="menu-item">
					
										
												</ul>
												<ul class="sub-menu-container mega-menu-column col">
													<li class="menu-item">
														<a class="menu-link" href=""><div>Synthetic Stimulants</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>TCA</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href="l"><div>THC (Marijuana)</div></a>
													</li>
													<li class="menu-item">
														<a class="menu-link" href=""><div>Tramadol</div></a>
													</li>
													
												</ul>
											</div>
										</div>
									</div>
								</li>
								
								
							</ul>

						</nav><!-- #primary-menu end -->
						<form class="top-search-form" action="search.html" method="get">
							<input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter.." autocomplete="off">
						</form>

					</div>
				</div>
			</div>
			<div class="header-wrap-clone"></div>
		</header><!-- #header end -->



		<section id="slider" class="slider-element slider-parallax swiper_wrapper min-vh-60 min-vh-md-100 include-header" data-autoplay="4000" data-speed="650" data-loop="true">

			<div class="slider-inner">

				<div class="swiper-container swiper-parent">
					<div class="swiper-wrapper">
					
						<div class="swiper-slide dark">
							<div class="container">
								<div class="slider-caption slider-caption-center">
									<h2 data-animate="fadeInUp">Welcome to UATests.com!</h2>
									<p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="100">Drug Test Kits and Drug Testing Information.</p>
								</div>
							</div>
							<div class="swiper-slide-bg" style="background-image: url('images/slider/swiper/1.jpg');"></div>
						</div>
						
						<div class="swiper-slide">
							<div class="container">
								<div class="slider-caption">
									<h2 data-animate="fadeInUp">All OF THE TESTS!!!</h2>
									<p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="100">Urine, saliva, hair, sweat.  You name the test, we have it, at the best price guaranteed!</p>
								</div>
							</div>
							<div class="swiper-slide-bg" style="background-image: url('images/slider/swiper/3.jpg'); background-position: center top;"></div>
						</div>						
						
						
						<div class="swiper-slide dark">
							<div class="container">
								<div class="slider-caption slider-caption-center">
									<h2 data-animate="fadeInUp">Drug Information</h2>
									<p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="100">Your resource for drug information &amp; the tests you need!</p>
								</div>
							</div>
							<div class="swiper-slide-bg" style="background-image: url('images/slider/swiper/2.jpg');"></div>
						</div>				
													
					</div>
					<div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
					<div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
				</div>

				<a href="#" data-scrollto="#content" data-offset="100" class="one-page-arrow dark"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

			</div>
		</section>
		
	</div><!-- #wrapper end -->



